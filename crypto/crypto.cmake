include(${CMAKE_CURRENT_LIST_DIR}/objects/objects.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/md4/md4.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/md5/md5.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/sha/sha.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/mdc2/mdc2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/hmac/hmac.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ripemd/ripemd.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/whrlpool/whrlpool.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/des/des.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/aes/aes.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/rc2/rc2.cmake)
get_directory_property(DEFINES COMPILE_DEFINITIONS)
if(NOT DEFINES MATCHES "\\b(OPENSSL_NO_RC4)\\b")
    include(${CMAKE_CURRENT_LIST_DIR}/rc4/rc4.cmake)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/idea/idea.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bf/bf.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cast/cast.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/camellia/camellia.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/seed/seed.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/modes/modes.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bio/bio.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bn/bn.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ec/ec.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/rsa/rsa.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/dsa/dsa.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ecdsa/ecdsa.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/dh/dh.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ecdh/ecdh.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/dso/dso.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/engine/engine.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/buffer/buffer.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/stack/stack.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/lhash/lhash.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/rand/rand.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/err/err.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/evp/evp.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/asn1/asn1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/pem/pem.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/x509/x509.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/x509v3/x509v3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/conf/conf.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/txt_db/txt_db.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/pkcs7/pkcs7.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/pkcs12/pkcs12.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/comp/comp.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ocsp/ocsp.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ui/ui.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/krb5/krb5.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cms/cms.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/pqueue/pqueue.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ts/ts.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/srp/srp.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmac/cmac.cmake)

OpenSSL_sources(
    cryptlib.c
    mem.c
    mem_clr.c
    mem_dbg.c
#    cversion.c
    ex_data.c
    cpt_err.c
    ebcdic.c
    uid.c
    o_time.c
    o_str.c
    o_dir.c
    o_fips.c
    o_init.c
    fips_ers.c
)
