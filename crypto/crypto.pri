include($$PWD/objects/objects.pri)
include($$PWD/md4/md4.pri)
include($$PWD/md5/md5.pri)
include($$PWD/sha/sha.pri)
include($$PWD/mdc2/mdc2.pri)
include($$PWD/hmac/hmac.pri)
include($$PWD/ripemd/ripemd.pri)
include($$PWD/whrlpool/whrlpool.pri)
include($$PWD/des/des.pri)
include($$PWD/aes/aes.pri)

include($$PWD/rc2/rc2.pri)
!contains(DEFINES, OPENSSL_NO_RC4): include($$PWD/rc4/rc4.pri)

include($$PWD/idea/idea.pri)
include($$PWD/bf/bf.pri)
include($$PWD/cast/cast.pri)
include($$PWD/camellia/camellia.pri)
include($$PWD/seed/seed.pri)
include($$PWD/modes/modes.pri)
include($$PWD/bio/bio.pri)
include($$PWD/bn/bn.pri)
include($$PWD/ec/ec.pri)
include($$PWD/rsa/rsa.pri)
include($$PWD/dsa/dsa.pri)
include($$PWD/ecdsa/ecdsa.pri)
include($$PWD/dh/dh.pri)
include($$PWD/ecdh/ecdh.pri)
include($$PWD/dso/dso.pri)
include($$PWD/engine/engine.pri)
include($$PWD/buffer/buffer.pri)

include($$PWD/stack/stack.pri)
include($$PWD/lhash/lhash.pri)
include($$PWD/rand/rand.pri)
include($$PWD/err/err.pri)
include($$PWD/evp/evp.pri)
include($$PWD/asn1/asn1.pri)
include($$PWD/pem/pem.pri)
include($$PWD/x509/x509.pri)
include($$PWD/x509v3/x509v3.pri)
include($$PWD/conf/conf.pri)
include($$PWD/txt_db/txt_db.pri)
include($$PWD/pkcs7/pkcs7.pri)
include($$PWD/pkcs12/pkcs12.pri)
include($$PWD/comp/comp.pri)
include($$PWD/ocsp/ocsp.pri)
include($$PWD/ui/ui.pri)
include($$PWD/krb5/krb5.pri)
include($$PWD/cms/cms.pri)
include($$PWD/pqueue/pqueue.pri)
include($$PWD/ts/ts.pri)
include($$PWD/srp/srp.pri)
include($$PWD/cmac/cmac.pri)

SOURCES += \
    $$PWD/cryptlib.c \
    $$PWD/mem.c \
    $$PWD/mem_clr.c \
    $$PWD/mem_dbg.c \
#    $$PWD/cversion.c \
    $$PWD/ex_data.c \
    $$PWD/cpt_err.c \
    $$PWD/ebcdic.c \
    $$PWD/uid.c \
    $$PWD/o_time.c \
    $$PWD/o_str.c \
    $$PWD/o_dir.c \
    $$PWD/o_fips.c \
    $$PWD/o_init.c \
    $$PWD/fips_ers.c


