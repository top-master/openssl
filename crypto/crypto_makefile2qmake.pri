
sub_dirs = \
    objects \
    md4 \
    md5 \
    sha \
    mdc2 \
    hmac \
    ripemd \
    whrlpool \
    des \
    aes \
    rc2 \
    rc4 \
    idea \
    bf \
    cast \
    camellia \
    seed \
    modes \
    bn \
    ec \
    rsa \
    dsa \
    ecdsa \
    dh \
    ecdh \
    dso \
    engine \
    buffer \
    bio \
    stack \
    lhash \
    rand \
    err \
    evp \
    asn1 \
    pem \
    x509 \
    x509v3 \
    conf \
    txt_db \
    pkcs7 \
    pkcs12 \
    comp \
    ocsp \
    ui \
    krb5 \
    cms \
    pqueue \
    ts \
    srp \
    cmac

#sub_dirs = evp #test

for(dir, sub_dirs) {
    pri_path = $$PWD/$$dir/$${dir}.pri
    exists($$pri_path): next()
    !exists($$PWD/$$dir/Makefile): next()

    ## LIBSRC = $$fromfile($$PWD/$$dir/Makefile, LIBSRC) //can not parse makefile
    LINES = $$cat($$PWD/$$dir/Makefile, lines)

    unset(LIBSRC)
    unset(CONTINUES)
    for(LINE, LINES) {
        !isEmpty(CONTINUES)|contains(LINE,^[ \t]*LIBSRC.*$) {
            contains(LINE,.*\\\\$): CONTINUES = true
            else: unset(CONTINUES)

            LINE = $$eval($$list($$LINE))
            LINE = $$find(LINE,[a-zA-Z_]*\.c)
            for(FILE, LINE) {
                LIBSRC += $${FILE}\\
            }
        }
    }
    LIBSRC += $${LITERAL_HASH}END_OF_FILES

    PRI_CONTENT = VPATH+=\$\$PWD
    PRI_CONTENT += SOURCES+=\\ $$LIBSRC

    write_file($$pri_path, PRI_CONTENT)
}
