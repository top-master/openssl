#include <openssl/opensslconf.h>

#ifdef OPENSSL_FIPS
# include "fips_err.h"
#else
extern const char dummy_fips_ers[1] = {'\0'};
#endif
