include($$PWD/ccgost/ccgost.pri)

VPATH += $$PWD
SOURCES += \
    e_4758cca.c \
    e_aep.c \
    e_atalla.c \
    e_cswift.c \
    e_gmp.c \
    e_chil.c \
    e_nuron.c \
    e_sureware.c \
    e_ubsec.c \
    e_padlock.c \
    e_capi.c
