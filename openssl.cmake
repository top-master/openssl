
add_library(OpenSSL_core OBJECT "${CMAKE_CURRENT_LIST_DIR}/include/openssl/conf.h")

include_directories("${CMAKE_CURRENT_LIST_DIR}/crypto")
include_directories("${CMAKE_CURRENT_LIST_DIR}/include")

# Disable ASM for 64-bit archs, to avoid this link error
# libcrypto.a
if(ANDROID_ABI STREQUAL "x86_64"
        OR CMAKE_OSX_ARCHITECTURES MATCHES ".*x86_64.*")
    add_definitions("-D OPENSSL_NO_ASM")
endif()

if(WIN32)
    get_directory_property(DEFINES COMPILE_DEFINITIONS)
    if(NOT DEFINES MATCHES "\\b(_WIN32)\\b")
        add_definitions("-D _WIN32")
    endif()

    add_definitions(
        "-D _CRT_NON_CONFORMING_SWPRINTFS"
        "-D OPENSSL_THREADS"
        "-D OPENSSL_SYSNAME_WIN32"
        "-D WIN32_LEAN_AND_MEAN"
        "-D L_ENDIAN"
        "-D _CRT_SECURE_NO_DEPRECATE"
        "-D OPENSSL_NO_ASM"
        "-D OPENSSL_NO_GMP"
        "-D OPENSSL_NO_JPAKE"
        "-D OPENSSL_NO_KRB5"
        "-D OPENSSL_NO_CAPIENG"
        "-D OPENSSL_NO_MD2"
        "-D OPENSSL_NO_RFC3779"
        "-D OPENSSL_NO_STORE"
        "-D OPENSSL_NO_STATIC_ENGINE"
        "-D OPENSSL_BUILD_SHLIBCRYPTO"
        "-D MK1MF_BUILD"
    )

    if(CMAKE_SIZEOF_VOID_P EQUAL 8)
        add_definitions("-D DSO_WIN32")
    endif()
endif(WIN32)

if(POLICY CMP0076)
    cmake_policy(SET CMP0076 NEW)
endif()

function(OpenSSL_sources)
    foreach(arg IN LISTS ARGV)
        set(path "${CMAKE_CURRENT_LIST_DIR}/${arg}")
        if(EXISTS "${path}")
            target_sources(OpenSSL_core PRIVATE "${path}")
        else()
            message( SEND_ERROR "Could not find file: ${arg}" )
        endif()
    endforeach()
endfunction()

include(${CMAKE_CURRENT_LIST_DIR}/crypto/crypto.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ssl/ssl.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/engines/engines.cmake)
