
INCLUDEPATH += $$PWD/crypto
INCLUDEPATH += $$PWD/include

win32 {
    DEFINES *= _WIN32
    DEFINES += \
        _CRT_NON_CONFORMING_SWPRINTFS \
        OPENSSL_THREADS \
        OPENSSL_SYSNAME_WIN32 \
        WIN32_LEAN_AND_MEAN \
        L_ENDIAN \
        _CRT_SECURE_NO_DEPRECATE \
        OPENSSL_NO_ASM \
        OPENSSL_NO_GMP \
        OPENSSL_NO_JPAKE \
        OPENSSL_NO_KRB5 \
        OPENSSL_NO_CAPIENG \
        OPENSSL_NO_MD2 \
        OPENSSL_NO_RFC3779 \
        OPENSSL_NO_STORE \
        OPENSSL_NO_STATIC_ENGINE \
        OPENSSL_BUILD_SHLIBCRYPTO \
        MK1MF_BUILD

    is64bit: DEFINES += DSO_WIN32
    # For OpenSSL static-linking.
    LIBS += User32.lib AdvAPI32.lib

    #QMAKE_LFLAGS += /ignore:4221
} #win32

include($$PWD/crypto/crypto.pri)
include($$PWD/ssl/ssl.pri)
include($$PWD/engines/engines.pri)

#include($$PWD/crypto/asn1/asn1.pri)
#include($$PWD/crypto/ui/ui.pri)
#include($$PWD/crypto/conf/conf.pri)
#include($$PWD/crypto/des/des.pri)
#include($$PWD/crypto/bf/bf.pri)
#include($$PWD/crypto/idea/idea.pri)
#include($$PWD/crypto/camellia/camellia.pri)
#include($$PWD/crypto/rc4/rc4.pri)
#include($$PWD/crypto/aes/aes.pri)
#include($$PWD/crypto/modes/modes.pri)
#include($$PWD/crypto/seed/seed.pri)
#include($$PWD/crypto/rc2/rc2.pri)
#include($$PWD/crypto/cast/cast.pri)
#include($$PWD/crypto/md4/md4.pri)
