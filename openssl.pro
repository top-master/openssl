
#
# OpenSSL by Eric-Andrew-Young (ssleay), version 1.0.1c
#

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    $$PWD/libeay32.pro \
    $$PWD/ssleay32.pro
