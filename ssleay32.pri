#
# link to OpenSSL by Eric-Andrew-Young (ssleay32)
#

defineTest(linkToSSLEAY) {
    unset(path)
    ## Same as $$OUT_PWD but in root
    path = $$shadowed($$PWD)
    equals(path, $$PWD) {
        error(building in source-dir is not supported)
    }
    QTDIR_build: path = $$XD_BUILD_TREE/lib/openssl
    ## Add sub-directories.
    CONFIG(debug, debug|release) {
        path = $$path/debug
    } else {
        path = $$path/release
    }

    LIBS += -L\"$$path\"

    LIBS += -lssleay32

    export(LIBS)
}

linkToSSLEAY() # Declared above.
