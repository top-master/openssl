#
# OpenSSL by Eric-Andrew-Young (ssleay)
#
TEMPLATE = lib
CONFIG -= qt

TARGET = ssleay32

INCLUDEPATH += $$PWD/crypto
INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/

include($$PWD/libeay32.pri)

win32 {
    DEF_FILE += $$PWD/vsbuild/ssleay32.def

    DEFINES *= _WIN32 _WINDLL
    DEFINES += \
        _CRT_NON_CONFORMING_SWPRINTFS \
        OPENSSL_THREADS \
        OPENSSL_SYSNAME_WIN32 \
        WIN32_LEAN_AND_MEAN \
        L_ENDIAN \
        _CRT_SECURE_NO_DEPRECATE \
        OPENSSL_NO_ASM \
        OPENSSL_NO_GMP \
        OPENSSL_NO_JPAKE \
        OPENSSL_NO_KRB5 \
        OPENSSL_NO_CAPIENG \
        OPENSSL_NO_MD2 \
        OPENSSL_NO_RFC3779 \
        OPENSSL_NO_STORE \
        OPENSSL_NO_STATIC_ENGINE \
        OPENSSL_BUILD_SHLIBCRYPTO \
        MK1MF_BUILD

    DEFINES *= OPENSSL_USE_APPLINK OPENSSL_NO_RC5 OPENSSL_BUILD_SHLIBSSL

    is64bit: DEFINES += DSO_WIN32
} #win32

QTDIR_build {
    DESTDIR = $$XD_BUILD_TREE/lib/openssl
    CONFIG(debug, debug|release): DESTDIR = $$DESTDIR/debug
    else: DESTDIR = $$DESTDIR/release
}


SOURCES += \
    $$PWD/crypto/evp/evp_fips.c \
    $$PWD/crypto/evp/e_aes.c \
    $$PWD/crypto/evp/e_aes_cbc_hmac_sha1.c \
    $$PWD/crypto/modes/cbc128.c \
    $$PWD/crypto/modes/ccm128.c \
    $$PWD/crypto/modes/cfb128.c \
    $$PWD/crypto/modes/ctr128.c \
    $$PWD/crypto/modes/gcm128.c \
    $$PWD/crypto/modes/ofb128.c \
    $$PWD/crypto/modes/xts128.c \
    $$PWD/crypto/srp/srp_lib.c \
    $$PWD/crypto/srp/srp_vfy.c \
    $$PWD/ssl/bio_ssl.c \
    $$PWD/ssl/d1_both.c \
    $$PWD/ssl/d1_clnt.c \
    $$PWD/ssl/d1_enc.c \
    $$PWD/ssl/d1_lib.c \
    $$PWD/ssl/d1_meth.c \
    $$PWD/ssl/d1_pkt.c \
    $$PWD/ssl/d1_srtp.c \
    $$PWD/ssl/d1_srvr.c \
    $$PWD/ssl/kssl.c \
    $$PWD/ssl/s23_clnt.c \
    $$PWD/ssl/s23_lib.c \
    $$PWD/ssl/s23_meth.c \
    $$PWD/ssl/s23_pkt.c \
    $$PWD/ssl/s23_srvr.c \
    $$PWD/ssl/s2_clnt.c \
    $$PWD/ssl/s2_enc.c \
    $$PWD/ssl/s2_lib.c \
    $$PWD/ssl/s2_meth.c \
    $$PWD/ssl/s2_pkt.c \
    $$PWD/ssl/s2_srvr.c \
    $$PWD/ssl/s3_both.c \
    $$PWD/ssl/s3_clnt.c \
    $$PWD/ssl/s3_enc.c \
    $$PWD/ssl/s3_lib.c \
    $$PWD/ssl/s3_meth.c \
    $$PWD/ssl/s3_pkt.c \
    $$PWD/ssl/s3_srvr.c \
    $$PWD/ssl/ssl_algs.c \
    $$PWD/ssl/ssl_asn1.c \
    $$PWD/ssl/ssl_cert.c \
    $$PWD/ssl/ssl_ciph.c \
    $$PWD/ssl/ssl_err.c \
    $$PWD/ssl/ssl_err2.c \
    $$PWD/ssl/ssl_lib.c \
    $$PWD/ssl/ssl_rsa.c \
    $$PWD/ssl/ssl_sess.c \
    $$PWD/ssl/ssl_stat.c \
    $$PWD/ssl/ssl_txt.c \
    $$PWD/ssl/t1_clnt.c \
    $$PWD/ssl/t1_enc.c \
    $$PWD/ssl/t1_lib.c \
    $$PWD/ssl/t1_meth.c \
    $$PWD/ssl/t1_reneg.c \
    $$PWD/ssl/t1_srvr.c \
    $$PWD/ssl/tls_srp.c
